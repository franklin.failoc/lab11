package com.tecsup.petclinic.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jgomezm
 *
 */
@Repository
public interface VetRepository 
	extends CrudRepository<Vet, Long> {

	// Fetch vets by firstname
	List<Vet> findByFName(String first_name);

	// Fetch vets by lastName
	List<Vet> findByLName(String last_name);

}
