package com.tecsup.petclinic.service;

import java.util.List;


import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.PetNotFoundException;
import com.tecsup.petclinic.exception.VetNotFoundException;

/**
 * 
 * @author jgomezm
 *
 */
public interface VetService {

	/**
	 * 
	 * @param pet
	 * @return
	 */
	Vet create(Vet vet);

	/**
	 * 
	 * @param pet
	 * @return
	 */
	Vet update(Vet vet);

	/**
	 * 
	 * @param id
	 * @throws VetNotFoundException
	 */
	void delete(Long id) throws VetNotFoundException;

	/**
	 * 
	 * @param id
	 * @return
	 */
	Vet findById(long id) throws VetNotFoundException;

	/**
	 * 
	 * @param firstname
	 * @return
	 */
	List<Vet> findByFName(String firt_name);

	/**
	 * 
	 * @param lastname
	 * @return
	 */
	List<Vet> findByLName(String last_name);


	/**
	 * 
	 * @return
	 */
	Iterable<Vet> findAll();

}