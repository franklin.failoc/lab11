package com.tecsup.petclinic.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.domain.VetRepository;
import com.tecsup.petclinic.exception.VetNotFoundException;

/**
 * 
 * @author jgomezm
 *
 */
@Service
public class VetServiceImpl implements VetService {

	private static final Logger logger = LoggerFactory.getLogger(VetServiceImpl.class);
	
	@Autowired
	VetRepository vetRepository;
	
	@Override
	public Vet create(Vet vet) {
		return vetRepository.save(vet);
	}
	
	@Override
	public Vet update(Vet vet) {
		return vetRepository.save(vet);
	}
	
	@Override
	public void delete(Long id) throws VetNotFoundException{

		Vet vet = findById(id);
		vetRepository.delete(vet);

	}
	
	public Vet findById(long id) throws VetNotFoundException {

		Optional<Vet> vet = vetRepository.findById(id);

		if ( !vet.isPresent())
			throw new VetNotFoundException("Record not found...!");
			
		return vet.get();
	}
	
	@Override
	public List<Vet> findByFName(String first_name) {

		List<Vet> vets = vetRepository.findByFName(first_name);

		vets.stream().forEach(vet -> logger.info("" + vet));

		return vets;
	}
	
	@Override
	public List<Vet> findByLName(String last_name) {

		List<Vet> vets = vetRepository.findByLName(last_name);

		vets.stream().forEach(vet -> logger.info("" + vet));

		return vets;
	}
	
	@Override
	public Iterable<Vet> findAll() {
		
		// TODO Auto-generated 
		return vetRepository.findAll();
	
	}

	
}