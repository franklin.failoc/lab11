package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.tecsup.petclinic.domain.Vet;
import com.tecsup.petclinic.exception.VetNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class VetServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(VetServiceTest.class);

	@Autowired
	private VetService vetService;

	/**
	 * 
	 */
	@Test
	public void testFindVetById(){

		long ID = 1;
		String NAME = "James";
		Vet vet = null;
		
		try {
			
			vet = vetService.findById(ID);
			
		} catch (VetNotFoundException e) {
			fail(e.getMessage());
		}
		logger.info("" + vet);

		assertEquals(NAME, vet.getFirst_name());

	}

	/**
	 * 
	 */
	@Test
	public void testFindVetByFName() {

		String FIND_NAME = "James";
		int SIZE_EXPECTED = 1;

		List<Vet> vets = vetService.findByFName(FIND_NAME);

		assertEquals(SIZE_EXPECTED, vets.size());
	}
	
	@Test
	public void testFindVetByLName() {

		String LAST_NAME = "Carter";
		int SIZE_EXPECTED = 1;

		List<Vet> vets = vetService.findByFName(LAST_NAME);

		assertEquals(SIZE_EXPECTED, vets.size());
	}

	

	/**
	 *  To get ID generate , you need 
	 *  setup in id primary key in your
	 *  entity this annotation :
	 *  	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 */
	@Test
	public void testCreateVet() {

		String VET_NAME = "Carlos";
		String VET_LNAME = "Herrera";
		

		Vet vet = new Vet(VET_NAME, VET_LNAME);
		vet = vetService.create(vet);
		logger.info("" + vet);

		assertThat(vet.getId()).isNotNull();
		assertEquals(VET_NAME, vet.getFirst_name());
		assertEquals(VET_LNAME, vet.getLast_name());
		

	}

	/**
	 * 
	 */
	@Test
	public void testUpdateVet() {

		String VET_NAME = "Cris";
		String VET_LNAME = "Sanchez";
				
		long create_id = -1;

		String UP_VET_NAME = "Cris2";
		String UP_VET_LNAME = "Sanchez2";
		

		Vet vet = new Vet(VET_NAME, VET_LNAME);

		// Create record
		logger.info(">" + vet);
		Vet readVet = vetService.create(vet);
		logger.info(">>" + readVet);

		create_id = readVet.getId();

		// Prepare data for update
		readVet.setFirst_name(UP_VET_NAME);
		readVet.setLast_name(UP_VET_LNAME);

		// Execute update
		Vet upgradeVet = vetService.update(readVet);
		logger.info(">>>>" + upgradeVet);
		
		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradeVet.getId());
		assertEquals(UP_VET_NAME, upgradeVet.getFirst_name());
		assertEquals(UP_VET_LNAME, upgradeVet.getLast_name());
		

		
	}

	/**
	 * 
	 */
	@Test
	public void testDeleteVet() {

		String VET_NAME = "Cris";
		String VET_LNAME = "Sanchez";
		

		Vet vet = new Vet(VET_NAME, VET_LNAME);
		vet = vetService.create(vet);
		logger.info("" + vet);

		try {
			vetService.delete(vet.getId());
		} catch (VetNotFoundException e) {
			fail(e.getMessage());
		}
			
		try {
			vetService.findById(vet.getId());
			assertTrue(false);
		} catch (VetNotFoundException e) {
			assertTrue(true);
		} 				

	}
}
